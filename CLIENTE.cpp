#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <iostream>
#include <dos.h>
#include <time.h> 
#include <conio.h>

char SendBuff[1000],RecvBuff[1000];
using namespace std;

int main(int argc, char *argv[]){

  WSADATA wsaData;
  SOCKET conn_socket;
  struct sockaddr_in server;
  struct hostent *hp;
  int resp;
  
  //Inicializamos la DLL de sockets
  resp=WSAStartup(MAKEWORD(1,0),&wsaData);
  if(resp){
    printf("Error al inicializar socket\n");
    getchar();return -1;
  }

  char opcion;
  cout << " Si desea iniciar teclee 1 : ";
  cin >> opcion;

  if (opcion=1) { 
      //Obtenemos la IP del servidor... en este caso
  // localhost indica nuestra propia m�quina...
  hp=(struct hostent *)gethostbyname("server1");

  if(!hp){
    printf("No se ha encontrado servidor...\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  // Creamos el socket...
  conn_socket=socket(AF_INET,SOCK_STREAM, 0);
  if(conn_socket==INVALID_SOCKET) {
    printf("Error al crear socket\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  memset(&server, 0, sizeof(server)) ;
  memcpy(&server.sin_addr, hp->h_addr, hp->h_length);
  server.sin_family = hp->h_addrtype;
  server.sin_port = htons(6000);

  // Nos conectamos con el servidor...
  if(connect(conn_socket,(struct sockaddr *)&server,sizeof(server))==SOCKET_ERROR){
    printf("Fallo al conectarse con el servidor\n");
    closesocket(conn_socket);
    WSACleanup();getchar();return WSAGetLastError();
  }
  printf("Conexi�n establecida con: %s\n", inet_ntoa(server.sin_addr));

  strcpy(SendBuff," BIENVENIDO AL SERVIDOR 1!");
  //Enviamos y recibimos datos...
  printf("Enviando Mensaje... \n");
  send(conn_socket,SendBuff,sizeof(SendBuff),0);
  printf("Datos enviados: %s \n", SendBuff);

  printf("Recibiendo Mensaje... \n");
  recv(conn_socket,RecvBuff, sizeof(RecvBuff), 0);
  printf("Datos recibidos: %s \n", RecvBuff);

  getchar();
  Sleep(5000); // Retraso.
  // Cerramos el socket y liberamos la DLL de sockets
  closesocket(conn_socket);
  
   //Obtenemos la IP del servidor... en este caso
  // localhost indica nuestra propia m�quina...
  hp=(struct hostent *)gethostbyname("server2");

  if(!hp){
    printf("No se ha encontrado servidor...\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  // Creamos el socket...
  conn_socket=socket(AF_INET,SOCK_STREAM, 0);
  if(conn_socket==INVALID_SOCKET) {
    printf("Error al crear socket\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  memset(&server, 0, sizeof(server)) ;
  memcpy(&server.sin_addr, hp->h_addr, hp->h_length);
  server.sin_family = hp->h_addrtype;
  server.sin_port = htons(6000);

  // Nos conectamos con el servidor...
  if(connect(conn_socket,(struct sockaddr *)&server,sizeof(server))==SOCKET_ERROR){
    printf("Fallo al conectarse con el servidor\n");
    closesocket(conn_socket);
    WSACleanup();getchar();return WSAGetLastError();
  }
  printf("\n Conexi�n establecida con: %s\n", inet_ntoa(server.sin_addr));

  strcpy(SendBuff," BIENVENIDO AL SERVIDOR 2!");
  //Enviamos y recibimos datos...
  printf("Enviando Mensaje... \n");
  send(conn_socket,SendBuff,sizeof(SendBuff),0);
  printf("Datos enviados: %s \n", SendBuff);

  printf("Recibiendo Mensaje... \n");
  recv(conn_socket,RecvBuff, sizeof(RecvBuff), 0);
  printf("Datos recibidos: %s \n", RecvBuff);

  getchar();
  Sleep(1000); // Retraso.
  // Cerramos el socket y liberamos la DLL de sockets
  closesocket(conn_socket);
  
    //Obtenemos la IP del servidor... en este caso
  // localhost indica nuestra propia m�quina...

  hp=(struct hostent *)gethostbyname("server3");

  if(!hp){
    printf("\n No se ha encontrado servidor...\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  // Creamos el socket...
  conn_socket=socket(AF_INET,SOCK_STREAM, 0);
  if(conn_socket==INVALID_SOCKET) {
    printf("Error al crear socket\n");
    getchar();WSACleanup();return WSAGetLastError();
  }

  memset(&server, 0, sizeof(server)) ;
  memcpy(&server.sin_addr, hp->h_addr, hp->h_length);
  server.sin_family = hp->h_addrtype;
  server.sin_port = htons(6000);

  // Nos conectamos con el servidor...
  if(connect(conn_socket,(struct sockaddr *)&server,sizeof(server))==SOCKET_ERROR){
    printf("Fallo al conectarse con el servidor\n");
    closesocket(conn_socket);
    WSACleanup();getchar();return WSAGetLastError();
  }
  printf("\n Conexi�n establecida con: %s\n", inet_ntoa(server.sin_addr));

  strcpy(SendBuff,"Bienvenido al servidor 3!");
  //Enviamos y recibimos datos...
  printf("Enviando Mensaje... \n");
  send(conn_socket,SendBuff,sizeof(SendBuff),0);
  printf("Datos enviados: %s \n", SendBuff);

  printf("Recibiendo Mensaje... \n");
  recv(conn_socket,RecvBuff, sizeof(RecvBuff), 0);
  printf("Datos recibidos: %s \n", RecvBuff);

  getchar();
 
  // Cerramos el socket y liberamos la DLL de sockets
  closesocket(conn_socket); 
   
}  

  cout <<"Saliendo del programa" << endl ; system("pause");
return 0;
  WSACleanup();
  
  //return EXIT_SUCCESS;
  
}

